package com.bsa.springdata.user;

import com.bsa.springdata.user.dto.UserDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findByLastNameStartingWithIgnoreCase(String lastName, Pageable pageable);

    @Query("select u from User u where u.office.city = :city order by u.lastName asc")
    List<User> findByCity(String city);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query("select u from User u where u.office.city = :city and u.team.room = :room")
    List<User> findUsersByRoomAndCity(@Param("city") String city, @Param("room") String room, Sort sort);

    @Modifying
    int deleteUsersByExperienceLessThan(int experience);

}
