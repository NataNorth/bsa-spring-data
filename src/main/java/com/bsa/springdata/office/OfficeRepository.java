package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    @Query("select distinct o from Office o left join o.users u where u.team.technology.name = :technology")
    List<Office> getByTechnology(@Param("technology") String technology);

    @Transactional
    @Modifying
    @Query("update Office o set o.address = :new" +
            " where exists (select p from Office o inner join o.users u inner join u.team.project p " +
            "where o.address =:old)" +
            "and o.address = :old")
    int updateOffice(@Param("old") String oldAddress, @Param("new") String newAddress);

    Office findOfficeByAddress(String address);
}
