package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    @Query("select t from Team t where size(t.users) < :userNum")
    List<Team> findAllByUsersSize(int userNum);

    @Transactional
    @Modifying
    @Query("update Team t set t.technology = :tech " +
            "where t in (select t from Team t where size(t.users) < :userNum and t.technology.name = :old)")
    void updateTechnology(@Param("old") String oldTechnologyName,
                          @Param("tech") Technology tech, @Param("userNum") int userNum);

    @Transactional
    @Modifying
    @Query(value = "UPDATE teams " +
            "SET name = CONCAT (team.name, '_', p.name, '_', tech.name) " +
            "FROM teams team " +
            "INNER JOIN projects p ON team.project_id = p.id " +
            "INNER JOIN technologies tech ON team.technology_id = tech.id " +
            "WHERE teams.name = :teamName", nativeQuery = true)
    void normalizeName(String teamName);


    int countByTechnologyName(String newTechnology);

    Optional<List<Team>> findByName(String teamName);
}
