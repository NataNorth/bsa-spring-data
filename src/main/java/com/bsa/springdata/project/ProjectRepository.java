package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query ("SELECT p FROM Team t LEFT JOIN t.project p LEFT JOIN t.technology tech " +
            "WHERE tech.name = :technology ORDER BY size(p.teams)")
    List<Project> findTop5ByTechnology(@Param("technology") String technology, Pageable pageable);

    @Query("select p " +
            "from Project p " +
            "inner join p.teams t "+
            "order by size(p.teams) desc, size(t.users) desc, p.name desc")
    Page<Project> findTheBiggest(Pageable pageable);

    @Query(value = "SELECT p.name as name," +
            "COUNT(DISTINCT t.id) as teamsNumber," +
            "COUNT(u.id) as developersNumber, " +
            "STRING_AGG ( DISTINCT tech.name, ','" +
            " ORDER BY tech.name DESC) technologies " +
            "FROM projects p " +
            "INNER JOIN teams t ON t.project_id = p.id " +
            "INNER JOIN users u ON t.id = u.team_id " +
            "INNER JOIN technologies tech ON t.technology_id = tech.id " +
            "GROUP BY p.id " +
            "ORDER BY p.name ", nativeQuery = true)
    List<ProjectSummaryDto> getProjectSummaryDto();

    @Query("select COUNT(p) from Project p " +
            "where exists (select u from p.teams t " +
            "inner join t.users u inner join u.roles r where r.name = :role)")
    int findCountWithRole(@Param("role") String role);
}